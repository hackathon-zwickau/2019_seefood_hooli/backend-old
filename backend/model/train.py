from keras import Model
from keras.layers import Dense, GlobalAveragePooling2D,Dropout
from keras.callbacks import TensorBoard,ModelCheckpoint
from keras.preprocessing.image import ImageDataGenerator
from keras.applications.mobilenet import preprocess_input, MobileNet

# Bild-Input Größe
input_shape = [224, 224, 3]
# Wie viele Bilder gleichzeitig für eine Epoche verwendet werden
_batch_size = 64
# Klassenzahl
classes = 14
# Verzeichnis mit Trainingsdaten, subset vom food101 Datensatz wurde verwendet: https://www.vision.ee.ethz.ch/datasets_extra/food-101/
train_dir = 'path/to/your/images'
# Starte Tensorboard mit: $ tensorboard --logdir=C:\Users\Felix\Downloads\minifood\logs
tensorboard_dir = 'path/to/your/logs'
# Verzeichnis in welchem das Model gespeichert wird
output_dir = 'path/to/your/model/seefood_mobilenet_food14_dropout_loss_optimized.h5'

# Lädt ein vortrainiertes Model
base_model = MobileNet(weights='imagenet', include_top=False)

# Definiert zusätzliche Layer
custom_model = base_model.output
custom_model = GlobalAveragePooling2D()(custom_model)
custom_model = Dense(1024, activation='relu')(custom_model)
custom_model = Dropout(0.25)(custom_model)
custom_model = Dense(512, activation='relu')(custom_model)
custom_model = Dropout(0.5)(custom_model)
predictions = Dense(classes, activation='softmax')(custom_model)

# Verbindet vortrainiertes Model und hängt die eigenen Schichten an
model = Model(inputs=base_model.input, outputs=predictions)

# Vordere Layer einfrieren um diese untrainierbar zu machen, hintere bleiben trainierbar
for layer in model.layers[:20]:
    layer.trainable = False
for layer in model.layers[20:]:
    layer.trainable = True

# verwende preprocess funktion für das mobilenet, teile daten zu 80% training und 20% validation auf.
# Zusätzlich wird ImageAugmentation angewandt
train_datagenerator = ImageDataGenerator(
    preprocessing_function=preprocess_input,
    validation_split=0.2,
    rotation_range=10,
    zoom_range=0.10,
    width_shift_range=0.15,
    height_shift_range=0.15,
    shear_range=0.05,
    fill_mode='nearest'
)

# Trainingsdaten einlesen
train_generator = train_datagenerator.flow_from_directory(
    train_dir,
    target_size=(input_shape[0], input_shape[1]),
    color_mode='rgb',
    batch_size=_batch_size,
    class_mode='categorical',
    shuffle=True,
    subset='training'
)

# Validationdaten einlesen
validation_generator = train_datagenerator.flow_from_directory(
    train_dir,
    target_size=(input_shape[0], input_shape[1]),
    subset='validation',
    color_mode='rgb',
    batch_size=_batch_size,
    shuffle=True,
)

# Legt optimizer, lossfunction und zu optimierende metrik fest, modell muss compiliert werden vor fitting/training
model.compile(optimizer='Adam', loss='categorical_crossentropy', metrics=['accuracy'])

# Erzeugt tensorboard log-files nach jeder epoche, ohne graph+images
tb_callback = TensorBoard(
    log_dir=tensorboard_dir,
    histogram_freq=1,
    update_freq='epoch',
    write_graph=False,
    write_images=False
)

# Speichert model nur dann, wenn validation_loss ein neues minimum erreicht
ckpt_callback = ModelCheckpoint(
    output_dir,
    monitor='val_loss',
    verbose=1,
    save_best_only=True,
    mode='min'
)

# wie viele steps pro epoche durchgeführt werden
step_size_train = train_generator.n // train_generator.batch_size
step_size_val = validation_generator.n // validation_generator.batch_size

# startet das training
model.fit_generator(
    callbacks=[ckpt_callback],
    generator=train_generator,
    validation_data=validation_generator,
    steps_per_epoch=step_size_train,
    validation_steps=step_size_val,
    epochs=50
)
