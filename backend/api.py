import os
import pymongo
from flask import Flask, request, abort
from keras.models import load_model
from keras.preprocessing import image
from keras.applications.mobilenet import preprocess_input
from numpy import array, newaxis
from bson.json_util import dumps

# keras model (224,244).. due to a typo
labels = ["chocolate_cake", "creme_brulee", "fish_and_chips", "greek_salad", "guacamole", "hotdog",
          "macaroni_and_cheese", "onion_rings", "_paella", "pho", "pizza", "spaghetti_bolognese", "spaghetti_carbonara",
          "tiramisu"]
model = load_model('/home/felixj/PycharmProjects/seefood-model/seefood_mobilenet_food14_loss_optimized.h5')
model.compile(optimizer='Adam', loss='categorical_crossentropy', metrics=['accuracy'])

# database
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["seefood-db"]
mycol = mydb["customers"]

# run api
api = Flask(__name__)

@api.route("/file/base64", methods=["POST"])
def post_base64_file():
    file = request.files['photo']
    _image = image.load_img(file, target_size=(224, 244,))
    result = predict(_image)
    return result, 200


@api.route("/recipe/<title>")
def get_recipe(title):
    mycol = mydb["recipes"]
    query = {"title": title}
    result = dumps(mycol.find(query))
    result = result.lstrip("[")
    result = result.rstrip("]")
    return result


@api.route("/nutrition/<name>")
def get_nutrition(name):
    # TODO: search for nutrition in mongodb by title
    return "my nutrition"


def predict(_image):
    x = image.img_to_array(_image)
    x = preprocess_input(x)
    y = array(x)[newaxis, :]
    prob = model.predict(y)
    _class = prob.argmax(axis=-1)
    return labels[_class[0]]


if __name__ == "__main__":
    api.run(port=8000, threaded=False)
