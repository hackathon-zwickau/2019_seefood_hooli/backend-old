import json
import pymongo

path = "/path/to/your/tiramisu.json"
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["seefood-db"]
recipe_col = mydb["recipes"]
# nutrition_col = mydb["nutrition"]

with open(path, "r") as read_file:
    data = json.load(read_file)
    title = data["title"]
    i = 0
    ingredients = []
    while i < len(data["ingredients"]):
        ingredient = data["quantity"][i]["text"]+" "+data["unit"][i]["text"]+" "+data["ingredients"][i]["text"]
        print(ingredient)
        ingredients.append(ingredient)
        i = i + 1
    i = 0
    instructions = []
    while i < len(data["instructions"]):
        instruction = data["instructions"][i]["text"]
        instructions.append(instruction)
        i = i +1

    recipe = {
        "title":title,
        "ingredients":ingredients,
        "instructions":instructions
    }
    print(recipe)
    key = {'_key': 'value'}
    recipe_col.replace_one(key, recipe, upsert=True)
